#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import kair_trajectory


traj2 = kair_trajectory.Spline3Trajectory.from_points_and_times([0, 1, 2], [0, 2, 1])

t = np.arange(0, 2, 0.01)
x = traj2.x(t)

dx = traj2.dx(t)
d2x = traj2.d2x(t)
d3x = traj2.d3x(t)
plt.figure(3)
plt.plot(t, x, 'b')
#plt.plot(t, dx, 'r')
#plt.plot(t, d2x, 'y')
#plt.plot(t, d3x, 'g')
plt.show()
