#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import kair_trajectory
    

traj1 = kair_trajectory.LinearTrajectory.from_points_and_times([0, 1, 2], [0, 1, 0])
t = np.arange(0, 2, 0.01)
x = traj1.x(t)
dx = traj1.dx(t)
plt.figure(2)
plt.plot(t, x, 'b', t, dx, 'r')
plt.grid()
plt.show()
