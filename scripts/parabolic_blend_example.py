#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import kair_trajectory


traj3 = kair_trajectory.ParabolicBlendTrajectory.from_points_and_times([0, 1, 2, 3, 4], [0, 1, 1.5, 1, 0], amax=2.0)
t = np.arange(0, 5, 0.01)
x = traj3.x(t)
dx = traj3.dx(t)
d2x = traj3.d2x(t)
#d3x = traj3.d3x(t)
plt.figure(4)
print len(t), len(x)
plt.plot(t, x, 'b')
plt.plot(t, dx, 'r')
plt.plot(t, d2x, 'y')
#plt.plot(t, d3x, 'g')
plt.show()

#print type(t), x, type(x)
