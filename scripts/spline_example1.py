#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import kair_trajectory
    

s1 = kair_trajectory.Spline3Segment.from_ends(0, 0, 0, 1, 1, 0)
t = np.arange(0, 1, 0.01)
x = s1.x(t)
dx = s1.dx(t)
d2x = s1.d2x(t)
plt.figure(1)
plt.plot(t, x, 'b', t, dx, 'r', t, d2x, 'y')
plt.show()
