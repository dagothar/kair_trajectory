#!/usr/bin/python3

import rospy
import kair_trajectory
import numpy as np
import matplotlib.pyplot as plt


T = kair_trajectory.SCurveTrajectory.from_points_and_times([0, 1, 2, 3, 4], [0, 1, 1.5, 1, 0], amax=2.5, jmax=10) #, amax=50, jmax=200)
t_end = T.duration()

#print T._segments

t = np.arange(0, t_end, 0.01)
x = T.x(t)
dx = T.dx(t)
d2x = T.d2x(t)
d3x = T.d3x(t)

plt.plot(t, x, 'b')
plt.plot(t, dx, 'r')
plt.plot(t, d2x, 'y')
plt.plot(t, d3x, 'k')
plt.xlim(0, t_end)
plt.ylim(-3, 3)
plt.grid()
plt.show()

