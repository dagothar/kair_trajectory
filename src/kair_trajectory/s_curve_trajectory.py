import numpy as np
import math
from .segmented_trajectory import SegmentedTrajectory
from .constant_velocity_segment import ConstantVelocitySegment
from .constant_acceleration_segment import ConstantAccelerationSegment
from .constant_jerk_segment import ConstantJerkSegment


class SCurveTrajectory(SegmentedTrajectory):
  """Defines a linear trajecory with jerk-limited blends."""
  
  def __init__(self):
    """Constructor."""
    
    SegmentedTrajectory.__init__(self)
    
  @classmethod
  def from_points_and_times(cls, t, x, amax=10.0, jmax=100.0):
    """Constructs a S-curve trajectory out of points and times.
    
    Straigth segment velocities are calculated based on the times between
    the consecutive points. The length of the blend segments is calculated
    based on the acceleration limit and the jerk limit.
    
    Parameters:
      t: np.array
        Vector of times. Should be monotonically increasing.
      x: np.array
        Vector of positions. Should be same length as t.
      amax: float, optional
        Acceleration limit.
      jmax: float, optional
        Jerk limit.
    """
    
    n = len(t)
    assert len(x) == n
    
    # compute speeds
    vs = [0.0]
    for i in range(0, n-1):
      vi = (x[i+1] - x[i]) / (t[i+1] - t[i])
      vs.append(vi)
    vs.append(0.0)
  
    # compute blend times
    tbs = []      # time of constant acceleration segment
    tjon = []     # time of jerk on segment
    tjoff = []    # time of jerk off segment
    accs = []     # signed accelerations
    jrks1 = []    # signed jerks
    jrks2 = []    # signed jerks
    Ts = []
    for i in range(0, len(vs)-1):
      
      dv = vs[i+1]-vs[i]
      ai = np.sign(dv)*amax
      accs.append(ai)
      jrk = np.sign(dv)*jmax
      
      T = 0.0
      tau = 0.0
      if abs(dv) <= (1.0*amax**2) / (jmax):
        T = 2*math.sqrt(abs(dv / jmax))
        tau = T / 2.0
        tbs.append(0.0)
        
      else:
        T = 1.0 * (abs(dv) - 1.0*amax**2 / jmax) / amax
        tau = 1.0 * amax / jmax
        #print T, tau
        tbi = T
        #assert tbi > 0
        tbs.append(tbi)
      
      Ts.append(T)
      tjon.append(tau)
      jrks1.append(jrk)
      tjoff.append(tau)
      jrks2.append(-jrk)
      
    
    # compute straight segment times
    ts = []
    vels = []
    for i in range(0, n-1):
      ti = t[i+1]-t[i]
      ti = ti - Ts[i]/2 - Ts[i+1]/2
      if ti < 0:
        raise RuntimeError("Duration of segment {} too small!".format(i+1))
      ts.append(ti)
      vi = vs[i+1]
      vels.append(vi)
    
    # construct trajectory
    traj = cls()
    x0 = x[0]
    v0 = 0.0
    t0 = 0.0
    a0 = 0.0
    for i in range(0, n):
      # Add the leading jerk segment
      jon = ConstantJerkSegment(x0, v0, 0.0, jrks1[i], t0, t0+tjon[i])
      traj.add_segment(jon)
      t0 = t0+tjon[i]
      x0 = jon.x(np.array([t0]))
      v0 = jon.dx(np.array([t0]))
      a0 = jon.d2x(np.array([t0]))
      
      #print "j={} t={}".format(jrks1[i], tjon[i])
      
      # Add the constant acceleration segment
      if tbs[i] > 0:
        blend = ConstantAccelerationSegment(x0, v0, a0, t0, t0+tbs[i])
        traj.add_segment(blend)
        t0 = t0+tbs[i]
        x0 = blend.x(np.array([t0]))
        v0 = blend.dx(np.array([t0]))
        a0 = blend.d2x(np.array([t0]))
        
        #print "a={} t={}".format(a0, tbs[i])
      
      # Add the trailing jerk segment
      joff = ConstantJerkSegment(x0, v0, a0, jrks2[i], t0, t0+tjoff[i])
      traj.add_segment(joff)
      t0 = t0+tjoff[i]
      x0 = joff.x(np.array([t0]))
      v0 = joff.dx(np.array([t0]))
      a0 = joff.d2x(np.array([t0]))
      
      #print "j={} t={}".format(jrks2[i], tjoff[i])
      
      if i < n-1:
        coast = ConstantVelocitySegment(x0, vels[i], t0, t0+ts[i])
        traj.add_segment(coast)
        t0 = t0+ts[i]
        x0 = coast.x(np.array([t0]))
        v0 = coast.dx(np.array([t0]))
    
        #print "v={} t={}".format(v0, ts[i])
    
    #print vs, accs
    #print 
    #print tjon
    return traj
