import numpy as np
from .segmented_trajectory import SegmentedTrajectory
from .constant_velocity_segment import ConstantVelocitySegment


class LinearTrajectory(SegmentedTrajectory):
  """Defines a trajectory made up of linear segments."""
  
  def __init__(self):
    """Constructor."""
    
    SegmentedTrajectory.__init__(self)
  
  @classmethod
  def from_points_and_times(cls, t, x):
    """Constructs the linear trajectory out of points and times.
    
    Parameters:
      t: np.array
        Vector of times. Should be monotonically increasing.
      x: np.array
        Vector of positions. Should be same length as t.
    """
    
    assert len(t) > 0
    assert len(t) == len(x)
    n = len(t)-1 # number of segments

    traj = cls()
    for i in range(0, n):
      t0 = t[i]
      t1 = t[i+1]
      dt = t1 - t0
      
      x0 = x[i]
      x1 = x[i+1]
      dx = x1 - x0
      
      v = dx / dt
      
      s = ConstantVelocitySegment(x0, v, t0, t1)
      traj._segments.append(s)
    
    return traj
