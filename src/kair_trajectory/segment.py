import numpy as np


class Segment:
  """Defines a trajectory segment."""
  
  def __init__(self, t0=0.0, t1=1.0):
    """Constructor.
    
    Parameters:
      t0 : float, optional
        Start time.
      t1: float, optional
        End time.
    """
    
    self._t0 = t0
    self._t1 = t1
  
  def duration(self):
    """Returns the duration of the segment.
    
    The duration is t1 - t0.
    """
    
    return self._t1 - self._t0

  def x(self, t):
    """Computes the values of the trajectory segment at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    
    xs = np.zeros(len(t))
    return xs
  
  def dx(self, t):
    """Computes the velocities of the trajectory segment at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      dxs: np.array
    """
    
    dxs = np.zeros(len(t))
    return dxs
  
  def d2x(self, t):
    """Computes the accelerations of the trajectory segment at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      d2xs: np.array
    """
    
    d2xs = np.zeros(len(t))
    return d2xs
  
  def d3x(self, t):
    """Computes the jerks of the trajectory segment at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      d3xs: np.array
    """
    
    d3xs = np.zeros(len(t))
    return d3xs
