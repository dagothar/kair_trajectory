import numpy as np


class Trajectory:
  """Defines a trajectory."""
  
  def __init__(self):
    """Constructor."""
    pass
  
  def t_start(self):
    """Returns the starting time of the trajectory."""
    return 0.0
  
  def t_end(self):
    """Returns the end time of the trajectory."""
    return 0.0
  
  def duration(self):
    """Returns the duration of the trajectory."""
    return self.t_end() - self.t_start()
  
  def x(self, t):
    """Computes the values of the trajectory at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    
    xs = np.zeros(len(t))
    return xs
  
  def dx(self, t):
    """Computes the velocities of the trajectory at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      dxs: np.array
    """
    
    dxs = np.zeros(len(t))
    return dxs
  
  def d2x(self, t):
    """Computes the accelerations of the trajectory at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      d2xs: np.array
    """
    
    d2xs = np.zeros(len(t))
    return d2xs
  
  def d3x(self, t):
    """Computes the jerks of the trajectory at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      d3xs: np.array
    """
    
    d3xs = np.zeros(len(t))
    return d3xs
