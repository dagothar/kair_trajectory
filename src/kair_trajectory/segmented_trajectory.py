import numpy as np
from .trajectory import Trajectory


class SegmentedTrajectory(Trajectory):
  """Defines a trajectory made of segments."""
  
  def __init__(self):
    """Constructor."""
    Trajectory.__init__(self)
    self._segments = []
  
  def add_segment(self, seg):
    """Adds a segment to the trajectory.
    
    Parameters:
      seg: Segment
        A segment to be added. The segment should have the start and end time values
        set up such that it is placed within the trajectory duration.
    """
    self._segments.append(seg)
    
  def t_start(self):
    """Returns the starting time of the trajectory."""
    return 0.0 if not self._segments else self._segments[0]._t0
    
  def t_end(self):
    """Returns the end time of the trajectory."""
    return 0.0 if not self._segments else self._segments[-1]._t1
    
  def x(self, t):
    """Computes the values of the trajectory at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    xs = np.zeros(t.shape)
    for i in range(len(t)):
      x = 0.0
      s = self.find_segment(t[i])
      if s:
        x = s.x(np.array([t[i]]))
      else:
        if t[i] < self.t_start() and self._segments:
          x = self._segments[0].x(self.t_start())
        if t[i] > self.t_end() and self._segments:
          x = self._segments[-1].x(self.t_end())
      xs[i] = x
    return xs
  
  def dx(self, t):
    """Computes the velocities of the trajectory at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      dxs: np.array
    """
    dxs = np.zeros(t.shape)
    for i in range(len(t)):
      dx = 0.0
      s = self.find_segment(t[i])
      if s:
        dx = s.dx(np.array([t[i]]))
      dxs[i] = dx
    return dxs
  
  def d2x(self, t):
    """Computes the accelerations of the trajectory at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      d2xs: np.array
    """
    d2xs = np.zeros(t.shape)
    for i in range(len(t)):
      d2x = 0.0
      s = self.find_segment(t[i])
      if s:
        d2x = s.d2x(np.array([t[i]]))
      d2xs[i] = d2x
    return d2xs
    
  def d3x(self, t):
    """Computes the jerks of the trajectory at times t.
    
    Parameters:
      t : np.array
      
    Returns:
      d3xs: np.array
    """
    d3xs = np.zeros(t.shape)
    for i in range(len(t)):
      d3x = 0.0
      s = self.find_segment(t[i])
      if s:
        d3x = s.d3x(np.array([t[i]]))
      d3xs[i] = d3x
    return d3xs
  
  def find_segment(self, t):
    """
    Returns segment appropriate for the time t.
    
    Returns None if no matching segment found.
    
    Parameters:
      t : float, time in seconds
    
    Returns:
      s : Segment
    """
    for s in reversed(self._segments):
      if s._t0 <= t and t <= s._t1:
        return s
    return None
    
