import numpy as np
from .segment import Segment


class ConstantVelocitySegment(Segment):
  """Defines a constant velocity trajectory segment."""
  
  def __init__(self, x0, v, t0=0.0, t1=1.0):
    """Constructor.
    
    Parameters:
      x0: float
        Position at the beginning of the segment.
      v: float
        Constant velocity throughout the segment.
      t0: float, optional
        Starting time.
      t1: float, optional
        End time.
    """
    
    Segment.__init__(self, t0, t1)
    self._x0 = x0
    self._v = v
  
  def x(self, t):
    """Computes the values of the trajectory segment at times t.
    
    x = x0 + v * t
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    return self._x0 + self._v*(t-self._t0)
  
  def dx(self, t):
    """Computes the velocities of the trajectory segment at times t.
    
    Returns a np.array of constant velocities.
    
    Parameters:
      t : np.array
      
    Returns:
      dxs: np.array
    """
    return self._v*np.ones(len(t))
