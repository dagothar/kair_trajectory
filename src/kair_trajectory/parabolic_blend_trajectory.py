import numpy as np
from .segmented_trajectory import SegmentedTrajectory
from .constant_velocity_segment import ConstantVelocitySegment
from .constant_acceleration_segment import ConstantAccelerationSegment


class ParabolicBlendTrajectory(SegmentedTrajectory):
  """Defines a linear trajecory with parabolic blends."""
  
  def __init__(self):
    """Constructor."""
    
    SegmentedTrajectory.__init__(self)
    
  @classmethod
  def from_points_and_times(cls, t, x, amax=10.0):
    """Constructs a parabolic blend trajectory out of points and times.
    
    Straigth segment velocities are calculated based on the times between
    the consecutive points. The length of the blend segments is calculated
    based on the acceleration limit.
    
    Parameters:
      t: np.array
        Vector of times. Should be monotonically increasing.
      x: np.array
        Vector of positions. Should be same length as t.
      amax: float, optional
        Acceleration limit.
    """
    
    n = len(t)
    assert len(x) == n
    
    # compute speeds
    vs = [0.0]
    for i in range(0, n-1):
      vi = (x[i+1] - x[i]) / (t[i+1] - t[i])
      vs.append(vi)
    vs.append(0.0)
  
    # compute blend times
    tbs = []
    accs = []
    for i in range(0, len(vs)-1):
      tbi = abs((vs[i+1]-vs[i]) / amax)
      tbs.append(tbi)
      ai = np.sign(vs[i+1]-vs[i])*amax
      accs.append(ai)
    
    # compute straight segment times
    ts = []
    vels = []
    for i in range(0, n-1):
      ti = t[i+1]-t[i]
      ti = ti - tbs[i]/2 - tbs[i+1]/2
      if ti < 0:
        raise RuntimeError("Duration of segment {} too small!".format(i+1))
      ts.append(ti)
      vi = vs[i+1]
      vels.append(vi)
    
    # construct trajectory
    traj = cls()
    x0 = x[0]
    v0 = 0.0
    t0 = 0.0
    for i in range(0, n):
      blend = ConstantAccelerationSegment(x0, v0, accs[i], t0, t0+tbs[i])
      traj.add_segment(blend)
      t0 = t0+tbs[i]
      x0 = blend.x(np.array([t0]))
      v0 = blend.dx(np.array([t0]))
      if i < n-1:
        coast = ConstantVelocitySegment(x0, vels[i], t0, t0+ts[i])
        traj.add_segment(coast)
        t0 = t0+ts[i]
        x0 = coast.x(np.array([t0]))
        v0 = coast.dx(np.array([t0]))
    
    return traj
