import numpy as np
from .segment import Segment


class Spline3Segment(Segment):
  """Defines a cubic spline trajectory segment."""
  
  def __init__(self, a0, a1, a2, a3, t0=0.0, t1=1.0):
    """Constructor.
    
    Constructs the spline segment x = a0 + a1 t + a2 t^2 + a3 t^3
    from the [a0, a1, a2, a3] coefficients.
    
    Parameters:
      a0, a1, a2, a3: float
        Spline coefficients.
      t0: float, optional
        Start time.
      t1: float, optional
        End time.
    """
    
    Segment.__init__(self, t0, t1)
    self._a0 = a0
    self._a1 = a1
    self._a2 = a2
    self._a3 = a3
  
  @classmethod
  def from_ends(cls, t1, x1, dx1, t2, x2, dx2):
    """Constructs the spine segment from the boundary conditions.
    
    Parameters:
      t1: float
        Start time.
      x1: float
        Position at the beginning.
      dx1: float
        Speed at the beginning.
      t2: float
        End time.
      x2: float
        Position at the end.
      dx2: float
        Speed at the end.
    """
    
    T = np.matrix([[1, t1, t1**2, t1**3], [0, 1, 2*t1, 3*t1**2], [1, t2, t2**2, t2**3], [0, 1, 2*t2, 3*t2**2]])
    X = np.matrix([[x1], [dx1], [x2], [dx2]])
    A = np.linalg.inv(T) * X
    return cls(float(A[0]), float(A[1]), float(A[2]), float(A[3]), t1, t2)
  
  def x(self, t):
    """Computes the values of the trajectory segment at times t.
    
    x = a0 + a1 t + a2 t^2 + a3 t^3
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    return self._a0 + self._a1*t + self._a2*np.power(t, 2) + self._a3*np.power(t, 3)
  
  def dx(self, t):
    """Computes the velocities of the trajectory segment at times t.
    
    x = a1 + 2 a2 t + 3 a3 t^2
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    return self._a1 + 2*self._a2*t + 3*self._a3*np.square(t)
  
  def d2x(self, t):
    """Computes the accelerations of the trajectory segment at times t.
    
    a = 2 a2 + 6 a3 t
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    
    return 2*self._a2 + 6*self._a3*t
  
  def d3x(self, t):
    """Computes the jerks of the trajectory segment at times t.
    
    j = 6 a3
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    
    return 6*self._a3
