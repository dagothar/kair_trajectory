import numpy as np
from .segmented_trajectory import SegmentedTrajectory
from .spline3_segment import Spline3Segment


class Spline3Trajectory(SegmentedTrajectory):
  """Defines a trajectory made up of cubic spline segments."""
  
  def __init__(self):
    """Constructor."""
    
    SegmentedTrajectory.__init__(self)
  
  @classmethod
  def from_points_and_times(cls, t, x, v0=0.0, v1=0.0):
    """Constructs the spline trajectory out of points and times.
    
    Intermediate velocities are calculated based on the velocity and acceleration
    continuity.
    
    Parameters:
      t: np.array
        Vector of times. Should be monotonically increasing.
      x: np.array
        Vector of positions. Should be same length as t.
      v0: float, optional
        Velocity at the beginning of the trajectory.
      v1: float, optional
        Velocity at the end of the trajectory.
    """
    
    assert len(t) > 0
    assert len(t) == len(x)
    n = len(t)-1 # number of spline segments

    traj = cls()
    # construct M matrix - add point and continuity conditions
    M = np.zeros([4*n, 4*n])
    for i in range(0, n):
      # point conditions
      M.itemset(4*i, 4*i, 1)
      M.itemset(4*i, 4*i+1, t[i])
      M.itemset(4*i, 4*i+2, t[i]**2)
      M.itemset(4*i, 4*i+3, t[i]**3)
      M.itemset(4*i+1, 4*i, 1)
      M.itemset(4*i+1, 4*i+1, t[i+1])
      M.itemset(4*i+1, 4*i+2, t[i+1]**2)
      M.itemset(4*i+1, 4*i+3, t[i+1]**3)
      
      # continuity conditions
      if i < n-1:
        # velocity
        M.itemset(4*i+2, 4*i, 0)
        M.itemset(4*i+2, 4*i+1, 1)
        M.itemset(4*i+2, 4*i+2, 2*t[i+1])
        M.itemset(4*i+2, 4*i+3, 3*t[i+1]**2)
        M.itemset(4*i+2, 4*i+4, 0)
        M.itemset(4*i+2, 4*i+5, -1)
        M.itemset(4*i+2, 4*i+6, -2*t[i+1])
        M.itemset(4*i+2, 4*i+7, -3*t[i+1]**2)
        
        # acceleration
        M.itemset(4*i+3, 4*i, 0)
        M.itemset(4*i+3, 4*i+1, 0)
        M.itemset(4*i+3, 4*i+2, 2)
        M.itemset(4*i+3, 4*i+3, 6*t[i+1])
        M.itemset(4*i+3, 4*i+4, 0)
        M.itemset(4*i+3, 4*i+5, 0)
        M.itemset(4*i+3, 4*i+6, -2)
        M.itemset(4*i+3, 4*i+7, -6*t[i+1])
      
    # add boundary velocity conditions
    M.itemset(4*n-2, 0, 0)
    M.itemset(4*n-2, 1, 1)
    M.itemset(4*n-2, 2, 2*t[0])
    M.itemset(4*n-2, 3, 3*t[0]**2)
    M.itemset(4*n-1, 4*n-4, 0)
    M.itemset(4*n-1, 4*n-3, 1)
    M.itemset(4*n-1, 4*n-2, 2*t[n])
    M.itemset(4*n-1, 4*n-1, 3*t[n]**2)
   
    # construct X matrix
    X = np.zeros([4*n, 1])
    for i in range(0, n):
      X.itemset(4*i, 0, x[i])
      X.itemset(4*i+1, 0, x[i+1])
      X.itemset(4*i+2, 0, 0)
      X.itemset(4*i+3, 0, 0)
    X.itemset(4*n-2, 0, v0)
    X.itemset(4*n-1, 0, v1)
    
    A = np.matmul(np.linalg.inv(M), X)
    for i in range(0, n):
      s = Spline3Segment(A[4*i], A[4*i+1], A[4*i+2], A[4*i+3], t[i], t[i+1])
      traj._segments.append(s)
    
    return traj
  
  @classmethod
  def from_points_and_velocities(cls, t, x, dx):
    """Constructs the spline trajectory out of points, times and intermediate velocities.
    
    Parameters:
      t: np.array
        Vector of times. Should be monotonically increasing.
      x: np.array
        Vector of positions. Should be same length as t.
      dx: np.array
        Vector of velocities. Should be same length as t.
    """
    
    assert len(t) > 1
    assert len(t) == len(x) == len(dx)
    n = len(t)
    traj = cls()
    for i in range(0, n-1):
      seg = Spline3Segment.from_ends(t[i], x[i], dx[i], t[i+1], x[i+1], dx[i+1])
      traj.add_segment(seg)
    return traj
