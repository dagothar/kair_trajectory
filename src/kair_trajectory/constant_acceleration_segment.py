import numpy as np
from .segment import Segment


class ConstantAccelerationSegment(Segment):
  """Defines a constant acceleration trajectory segment."""
  
  def __init__(self, x0, v0, a, t0=0.0, t1=1.0):
    """Constructor.
    
    Parameters:
      x0: float
        Position at the beginning of the segment.
      v0: float
        Velocity at the beginning of the segment.
      a: float
        Constant acceleration throughout the segment.
      t0: float, optional
        Starting time.
      t1: float, optional
        End time.
    """
    
    Segment.__init__(self, t0, t1)
    self._x0 = x0
    self._v0 = v0
    self._a = a
    
  def x(self, t):
    """Computes the values of the trajectory segment at times t.
    
    x = x0 + v0 * t + 1/2 a * t^2
    
    Parameters:
      t : np.array
      
    Returns:
      xs: np.array
    """
    
    return self._x0 + self._v0*(t-self._t0) + 0.5*self._a*np.power((t-self._t0), 2)
  
  def dx(self, t):
    """Computes the velocities of the trajectory segment at times t.
    
    v = v0 + a * t
    
    Parameters:
      t : np.array
      
    Returns:
      dxs: np.array
    """
    
    return self._v0 + self._a*(t-self._t0)
  
  def d2x(self, t):
    """Computes the accelerations of the trajectory segment at times t.
    
    Returns a np.array of constant accelerations.
    
    Parameters:
      t : np.array
      
    Returns:
      d2xs: np.array
    """
    
    return self._a*np.ones(len(t))
